# YALLM bot: yet another (Telegram) LLM bot

## Features

Sustains a conversation with a user. Powered by LLM APIs.

## Todo

- [X] Limit context length
- [ ] Add more commands
- [ ] Store per user settings
- [ ] Store per user conversations
- [ ] Add support for LLMs besides GPT.
