let
  nixpkgs = import
    (builtins.fetchTarball {
      name = "nixos-23.11";
      url = "https://github.com/NixOS/nixpkgs/archive/refs/tags/23.11.tar.gz";
      sha256 = "1ndiv385w1qyb3b18vw13991fzb9wg4cl21wglk89grsfsnra41k";
    })
    { };
  poetry2nix = import
    (nixpkgs.fetchFromGitHub {
      owner = "nix-community";
      repo = "poetry2nix";
      rev = "3c92540611f42d3fb2d0d084a6c694cd6544b609";
      sha256 = "sha256-2GOiFTkvs5MtVF65sC78KNVxQSmsxtk0WmV1wJ9V2ck=";
    })
    { };
in
with nixpkgs;
let
  poetryEnv = poetry2nix.mkPoetryEnv {
    projectDir = ./.;
    editablePackageSources =
      { path = ./.; };
    preferWheels = true;
  };
in
poetryEnv.env.overrideAttrs (oldAttrs: {
  nativeBuildInputs = oldAttrs.nativeBuildInputs ++ [
    python311Packages.setuptools
    python311Packages.setuptools-rust
  ];
  buildInputs = [
    kubernetes-helm
    nixpkgs-fmt
    poetry
  ];
})
