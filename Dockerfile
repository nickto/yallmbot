FROM python:3.11-alpine3.19

ARG YALLMBOT_UID=1000
ARG YALLMBOT_GID=1000

RUN apk add --no-cache \
  libc-dev==0.7.2-r5 \
  libffi-dev==3.4.4-r3 \
  gcc==13.2.1_git20231014-r0 \
  && pip install --no-cache-dir poetry==1.7.1

RUN addgroup -g $YALLMBOT_GID yallmbot && adduser -D -u $YALLMBOT_UID -G yallmbot yallmbot
USER yallmbot
WORKDIR /app
COPY  pyproject.toml poetry.toml poetry.lock README.md /app/
COPY yallmbot /app/yallmbot

RUN poetry install --without dev --compile
ENTRYPOINT [ "poetry", "run", "yallmbot" ]
