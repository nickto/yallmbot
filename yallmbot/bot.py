import logging
from pathlib import Path
from typing import Iterable, List

from telegram import Message as TelegramMessage
from telegram import Update
from telegram.constants import ChatType
from telegram.ext import (
    ApplicationBuilder,
    CommandHandler,
    ContextTypes,
    MessageHandler,
    filters,
)

from yallmbot.conversations import Conversation, Message, Role, User
from yallmbot.db import ConversationsDb
from yallmbot.llm import Gpt

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class GollemBot:
    db: ConversationsDb
    telegram_token: str
    openai_token: str
    blacklisted_usernames: List[str]
    whitelisted_usernames: List[str]

    def __init__(
        self,
        telegram_token: str,
        openai_token: str,
        sqlite_path: Path,
        blacklisted_usernames: Iterable[str] = (),
        whitelisted_usernames: Iterable[str] = (),
    ):
        self.telegram_token = telegram_token
        self.openai_token = openai_token
        self.sqlite_path = sqlite_path

        self.blacklisted_usernames = list(blacklisted_usernames)
        self.whitelisted_usernames = list(whitelisted_usernames)
        if len(self.blacklisted_usernames) > 0 and len(self.whitelisted_usernames) > 0:
            logger.warning(
                "Both blacklisted and whitelisted usernames are provided. "
                "Blacklisted will be ignored."
            )

        self.db = ConversationsDb(self.sqlite_path)

    def start(self):
        app = ApplicationBuilder().token(self.telegram_token).build()
        app.add_handler(CommandHandler("help", self.help))
        app.add_handler(CommandHandler("new", self.new))
        app.add_handler(MessageHandler(filters.TEXT & ~filters.COMMAND, self.reply))

        app.run_polling()

    def is_user_allowed(self, user: User) -> bool:
        if self.whitelisted_usernames:
            return user.username in self.whitelisted_usernames
        else:
            return user.username not in self.blacklisted_usernames

    async def short_circtuit_if_user_not_allowed(
        self, user: User, telegram_message: TelegramMessage
    ) -> bool:
        if not self.is_user_allowed(user):
            logger.debug(f"User {user.username} is not allowed to use the bot.")
            msg = (
                "Sorry, this bot is not available for you. "
                "Please, contact its owner if you want to use it."
            )
            await telegram_message.reply_text(msg)
            return True
        else:
            return False

    def get_user_from_telegram_message(self, telegram_message: TelegramMessage) -> User:
        # We know it is not None because we have filtered such cases
        assert telegram_message is not None

        user_dict = telegram_message.to_dict()["from"]
        user = User.model_validate(user_dict)
        return user

    def get_message_from_telegram_message(
        self, telegram_message: TelegramMessage
    ) -> Message:
        # We know it is not NOne because we have filtered such cases
        assert telegram_message is not None

        message_dict = telegram_message.to_dict()

        message_dict["user_id"] = message_dict["from"]["id"]

        if message_dict["from"]["is_bot"]:
            message_dict["role"] = Role.BOT
        else:
            message_dict["role"] = Role.USER

        message = Message.model_validate(message_dict)
        return message

    async def help(self, update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
        if update.message is None:
            # This happens when the message was edited, for example
            return

        msg = "Help is under development."
        await update.message.reply_text(msg)

    async def new(self, update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
        if update.message is None:
            # This happens when the message was edited, for example
            return

        user = self.get_user_from_telegram_message(update.message)
        if await self.short_circtuit_if_user_not_allowed(user, update.message):
            return

        await self.db.new_conversation(user)

        msg = "New conversation started."
        await update.message.reply_text(msg)

    async def get_conversation_messages(
        self, conversation: Conversation
    ) -> List[Message]:
        messages = await self.db.get_conversation_messages(conversation)
        return messages

    async def get_response_text(self, messages: List[Message]) -> str:
        # TODO: LLM type should come from configs
        llm = Gpt(token=self.openai_token)
        for message in messages:
            llm.add_message(message)

        msg = llm.generate_response()
        return msg

    async def reply(self, update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
        if update.message is None:
            # This happens when the message was edited, for example
            return

        if update.message.chat.type != ChatType.PRIVATE:
            # Ignore non-private messages
            return

        # Get objects from message
        user: User = self.get_user_from_telegram_message(update.message)
        if await self.short_circtuit_if_user_not_allowed(user, update.message):
            return

        # Get latest conversation ID
        conversation = await self.db.get_latest_conversation(user)

        if conversation is None:
            # /new was never called
            msg = "This is the first time we talk. Please use /new to start a conversation."
            await update.message.reply_text(msg)
            return

        message: Message = self.get_message_from_telegram_message(update.message)
        message.conversation_id = conversation.id
        await self.db.insert_message(message)

        messages = await self.get_conversation_messages(conversation)
        msg = await self.get_response_text(messages)
        telegram_message = await update.message.reply_text(msg)  # type: ignore

        bot_message = self.get_message_from_telegram_message(telegram_message)
        bot_message.conversation_id = conversation.id
        await self.db.insert_message(message)


def start(
    telegram_token: str,
    openai_token: str,
    sqlite_path: Path,
    blacklisted_usernames: List[str],
    whitelisted_usernames: List[str],
):
    bot = GollemBot(
        telegram_token,
        openai_token,
        sqlite_path,
        blacklisted_usernames,
        whitelisted_usernames,
    )
    bot.start()
