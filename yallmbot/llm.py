from __future__ import annotations

from abc import ABC, abstractmethod
from enum import Enum
from functools import lru_cache
from typing import Dict, List

import tiktoken
from openai import Client, OpenAI
from pydantic import BaseModel

from yallmbot.conversations import Message, Role

DEFAULT_INSTRUCTIONS = (
    "You are a helpful assistant. Answer as concisely and precisely as possible."
)


class LlmMessage(BaseModel):
    role: Role
    content: str


class Llm(ABC):
    messages: list[LlmMessage]

    def __init__(self):
        self.messages = []

    def add_message(self, message: Message) -> Llm:
        llm_message = LlmMessage(
            role=message.role,
            content=message.text,
        )
        self.messages.append(llm_message)
        return self

    @abstractmethod
    def generate_response(self, **kwargs) -> str:
        raise NotImplementedError


class GptModel(str, Enum):
    # See completions endpoint compatibility here:
    # https://platform.openai.com/docs/models/model-endpoint-compatibility
    GPT_3_5_TURBO = "gpt-3.5-turbo"
    GPT_3_5_TURBO_16K = "gpt-3.5-turbo-16k"
    GPT_4 = "gpt-4"
    GPT_4_32K = "gpt-4-32k"


class Gpt(Llm):
    instructions: str
    client: Client
    model: GptModel

    role_to_gpt_role: Dict[Role, str] = {
        Role.USER: "user",
        Role.BOT: "assistant",
        Role.INSTRUCTIONS: "system",
    }

    class TooLongInstructionsError(Exception):
        ...

    def __init__(
        self,
        token: str,
        model=GptModel.GPT_3_5_TURBO,
        instructions=DEFAULT_INSTRUCTIONS,
    ):
        super().__init__()
        self.client = OpenAI(api_key=token)
        self.model = model
        self.instructions = instructions

    def get_max_context_length(self) -> int:
        context_length: Dict[GptModel, int] = {
            # Up-to-date context lengths can be found here:
            # https://platform.openai.com/docs/models/overview
            GptModel.GPT_3_5_TURBO: 4_096,
            GptModel.GPT_3_5_TURBO_16K: 16_384,
            GptModel.GPT_4: 8_192,
            GptModel.GPT_4_32K: 32_768,
        }

        return context_length[self.model]

    @lru_cache(maxsize=1)
    def _get_encoding(self) -> tiktoken.Encoding:
        return tiktoken.encoding_for_model(self.model.value)

    def count_tokens(self, content: str) -> int:
        encoding = self._get_encoding()
        num_tokens = len(encoding.encode(content))
        return num_tokens

    def get_conversation_history(self) -> List[Dict[str, str]]:
        max_context_tokens = self.get_max_context_length()
        max_history_tokens = max_context_tokens // 2

        # First, add a system message (instructions)
        messages = [
            {
                "role": "system",
                "content": self.instructions,
            }
        ]
        history_tokens = self.count_tokens(self.instructions)
        if history_tokens > max_history_tokens:
            raise self.TooLongInstructionsError

        # Then keep adding messages from the back, until they do not exceed the
        # max_history_tokens
        for message in reversed(self.messages):
            mesage_tokens = self.count_tokens(message.content)
            if history_tokens + mesage_tokens > max_history_tokens:
                break
            messages.insert(
                1,  # 1, because inserting after the system message
                {
                    "role": self.role_to_gpt_role[message.role],
                    "content": message.content,
                },
            )

        return messages

    def generate_response(self) -> str:  # type: ignore
        try:
            messages = self.get_conversation_history()
        except self.TooLongInstructionsError:
            return (
                "Your instructions are too long for the model in use. "
                "Try making them shorter."
            )

        completion = self.client.chat.completions.create(
            messages=messages,  # type: ignore
            model="gpt-3.5-turbo",
        )
        response = completion.choices[0].message.content
        if response is None:
            return ""
        else:
            return response
