from datetime import datetime
from enum import Enum

from pydantic import AliasChoices, BaseModel, Field


class Role(str, Enum):
    USER = "user"
    BOT = "bot"
    INSTRUCTIONS = "instrcutions"


class Message(BaseModel):
    id: int = Field(validation_alias=AliasChoices("id", "message_id"))
    date: datetime
    text: str
    user_id: int
    conversation_id: int | None = Field(default=None)
    role: Role


class User(BaseModel):
    id: int
    first_name: str | None
    last_name: str | None
    username: str
    is_bot: bool
    language_code: str | None


class Conversation(BaseModel):
    id: int
    user_id: int
