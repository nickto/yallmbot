import logging
import logging.config
import os
from pathlib import Path
from typing import Optional

import typer
import yaml
from typing_extensions import Annotated

from yallmbot import bot

# This is an entrypoint, so name the logger explicitly,
# otherwise it will be named __main__
logger = logging.getLogger("yallmbot.main")


cli_app = typer.Typer()


@cli_app.command()
def start(
    sqlite_path: Annotated[
        Optional[Path],
        typer.Option(help="Path to SQLite database."),
    ] = None,
    blacklist_path: Annotated[
        Optional[Path],
        typer.Option(help="Path to YAML file containing blacklisted usernames."),
    ] = None,
    whitelist_path: Annotated[
        Optional[Path],
        typer.Option(help="Path to YAML file containing whitelisted usernames."),
    ] = None,
):
    LOG_CONFIG = os.getenv("LOG_CONFIG")
    if LOG_CONFIG is not None:
        log_config = yaml.safe_load(open(LOG_CONFIG, "r"))
        logging.config.dictConfig(log_config)

    TELEGRAM_API_TOKEN = os.getenv("TELEGRAM_API_TOKEN")
    if TELEGRAM_API_TOKEN is None:
        raise ValueError("TELEGRAM_API_TOKEN not set")

    OPENAI_API_TOKEN = os.getenv("OPENAI_API_TOKEN")
    if OPENAI_API_TOKEN is None:
        raise ValueError("OPENAI_API_TOKEN not set")

    # SQLite path
    if sqlite_path is None:
        # Check if present in env vars
        sqlite_path_str = os.getenv("DB_PATH")
        if sqlite_path_str is None:
            logger.info("Using default database path: db.sqlite")
            sqlite_path = Path("db.sqlite")
        else:
            msg = f"Using database path from environment variable: {sqlite_path_str}"
            logger.info(msg)
            sqlite_path = Path(sqlite_path_str)
    else:
        sqlite_path = Path(sqlite_path)
        logger.info(f"Using database path from environment variable: {sqlite_path}")

    # Blacklist path
    if blacklist_path is None:
        # Check if present in env vars
        blacklist_path_str = os.getenv("BLACKLIST_PATH")
        if blacklist_path_str is not None:
            blacklist_path = Path(blacklist_path_str)
    if blacklist_path is None:
        logger.info("No blacklist path provided")
        blacklisted_usernames = []
    else:
        try:
            with open(blacklist_path) as f:
                blacklisted_usernames = yaml.safe_load(f)
        except FileNotFoundError:
            logger.warning(f"Blacklist file not found: {blacklist_path}")
            blacklisted_usernames = []
    logger.info(f"Blacklisted username: {blacklisted_usernames}")

    # Whitelist path
    if whitelist_path is None:
        # Check if present in env vars
        whitelist_path_str = os.getenv("WHITELIST_PATH")
        if whitelist_path_str is not None:
            whitelist_path = Path(whitelist_path_str)
    if whitelist_path is None:
        logger.info("No whitelist path provided")
        whitelisted_usernames = []
    else:
        try:
            with open(whitelist_path) as f:
                whitelisted_usernames = yaml.safe_load(f)
        except FileNotFoundError:
            logger.warning(f"Whitelist file not found: {whitelist_path}")
            whitelisted_usernames = []
    logger.info(f"Whitelisted username: {whitelisted_usernames}")

    bot.start(
        telegram_token=TELEGRAM_API_TOKEN,
        openai_token=OPENAI_API_TOKEN,
        sqlite_path=sqlite_path,
        blacklisted_usernames=blacklisted_usernames,
        whitelisted_usernames=whitelisted_usernames,
    )
