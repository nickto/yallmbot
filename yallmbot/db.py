import logging
import sqlite3
from pathlib import Path
from typing import List

import aiosqlite

from yallmbot.conversations import Conversation, Message, User

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class ConversationsDb:
    path: Path

    def __init__(self, path: Path) -> None:
        self.path = path

        self.create_tables_if_needed()

    def get_connection(
        self, use_async: bool = True
    ) -> aiosqlite.Connection | sqlite3.Connection:
        if use_async:
            return aiosqlite.connect(self.path)
        else:
            return sqlite3.connect(self.path)

    def create_tables_if_needed(self):
        with self.get_connection(use_async=False) as conn:  # type: ignore
            logger.debug("Creating users table (if does not exist).")
            conn.execute(
                """
                CREATE TABLE IF NOT EXISTS users (
                    id INTEGER PRIMARY KEY,
                    first_name TEXT,
                    last_name TEXT,
                    username TEXT,
                    is_bot BOOLEAN,
                    language_code TEXT
                )
                """
            )
            logger.debug("Creating conversations table (if does not exist).")
            conn.execute(
                """
                CREATE TABLE IF NOT EXISTS conversations (
                    id INTEGER PRIMARY KEY,
                    user_id INTEGER,
                    FOREIGN KEY (user_id) REFERENCES users (id)
                )
                """
            )

            logger.debug("Creating messages table (if does not exist).")
            conn.execute(
                """
                CREATE TABLE IF NOT EXISTS messages (
                    id INTEGER,
                    date INTEGER,
                    text TEXT,
                    user_id INTEGER,
                    conversation_id INTEGER,
                    role TEXT,
                    FOREIGN KEY (user_id) REFERENCES users (id)
                    FOREIGN KEY (conversation_id) REFERENCES conversations (id)
                )
                """
            )

    async def insert_message(self, message: Message) -> None:
        if message.conversation_id is None:
            raise ValueError("Message must have a conversation_id when saving to db.")

        async with self.get_connection() as conn:  # type: ignore
            sql = """
                    INSERT INTO messages (
                        id, 
                        date, 
                        text, 
                        user_id, 
                        conversation_id,
                        role
                    ) 
                    VALUES (?, ?, ?, ?, ?, ?)
                """
            await conn.execute(
                sql,
                (
                    message.id,
                    message.date,
                    message.text,
                    message.user_id,
                    message.conversation_id,
                    message.role,
                ),
            )
            await conn.commit()

    async def insert_conversation(self, conversation: Conversation) -> None:
        logger.info("Creating new conversation for user %d.", conversation.user_id)
        async with self.get_connection() as conn:  # type: ignore
            sql = """
                INSERT INTO conversations (
                    id, 
                    user_id
                ) 
                VALUES (?, ?)
            """
            await conn.execute(
                sql,
                (
                    conversation.id,
                    conversation.user_id,
                ),
            )
            await conn.commit()

    async def new_conversation(self, user: User) -> None:
        # Find currently largest conversation id
        async with self.get_connection() as conn:  # type: ignore
            sql = """
                SELECT MAX(id) 
                FROM conversations
            """
            async with conn.execute(sql) as cursor:
                conversation_id = await cursor.fetchone()
                # conversation_id is a tuple: take the first element
                conversation_id = conversation_id[0]  # type: ignore

        # Get new conversation id
        if conversation_id is None:
            # Happens when there are no conversations yet
            conversation_id = 1
        else:
            conversation_id += 1

        conversation = Conversation(id=conversation_id, user_id=user.id)
        await self.insert_conversation(conversation)

    async def get_latest_conversation(self, user: User) -> Conversation | None:
        async with self.get_connection() as conn:  # type: ignore
            sql = """
                SELECT * 
                FROM conversations 
                WHERE user_id = ? 
                ORDER BY id DESC
            """
            async with conn.execute(sql, (user.id,)) as cursor:
                conversation = await cursor.fetchone()

            if conversation is None:
                return None

            return Conversation(
                id=conversation[0],
                user_id=conversation[1],
            )

    async def get_conversation_messages(
        self, conversation: Conversation
    ) -> List[Message]:
        async with self.get_connection() as conn:  # type: ignore
            sql = """
                SELECT *
                FROM messages
                WHERE 
                    conversation_id = ?
                    AND user_id = ?
                ORDER BY date
            """
            async with conn.execute(
                sql, (conversation.id, conversation.user_id)
            ) as cursor:
                messages = await cursor.fetchall()

        return [
            Message(
                id=message[0],
                date=message[1],
                text=message[2],
                user_id=message[3],
                conversation_id=message[4],
                role=message[5],
            )
            for message in messages
        ]
